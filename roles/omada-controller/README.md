```
  ___                      _
 / _ \ _ __ ___   __ _  __| | __ _
| | | | '_ ` _ \ / _` |/ _` |/ _` |
| |_| | | | | | | (_| | (_| | (_| |
 \___/|_| |_| |_|\__,_|\__,_|\__,_|


  ____            _             _ _
 / ___|___  _ __ | |_ _ __ ___ | | | ___ _ __
| |   / _ \| '_ \| __| '__/ _ \| | |/ _ \ '__|
| |__| (_) | | | | |_| | | (_) | | |  __/ |
 \____\___/|_| |_|\__|_|  \___/|_|_|\___|_|

```

Ansible role to install Omada Controller on Debian-based OS

Supports both x86_64 and arm64 architectures.

## Documentation

* [omada-controller rpi setup guide](https://community.tp-link.com/en/business/forum/topic/528450)

**Omada-Controller Install Info:**

* Omada-Controller directory: `/opt/tplink/EAPController`
* Controller takes ~6min to startup on boot (on rpi4)
* Controller will initially available at `x.x.x.x:8088`

## Arm64 Considerations

I was unable to find a `libssl_v1.1` pkg available and had to build it from source, which takes quite a while.  As such, the arm libssl_v1.1 deb pkg is included in the role.

## How to Run This Role

You must specify the `omadahost` var within `/ansible/env_vars/home/omada-controller.yml` (or update the playbook appropriately)

From the ansible root directory:

```
ansible-playbook playbooks/omada-controller.yml -vv
```

