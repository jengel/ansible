#!/usr/bin/env bash
set +xe

ansible-lint \
  --show-relpath \
  -x command-instead-of-shell,empty-string-compare,ignore-errors,literal-compare,no-changed-when,no-handler,no-relative-paths,package-latest,risky-file-permissions,risky-shell-pipe,role-name,var-naming \
  roles/ env_vars/ playbooks/ inventory

