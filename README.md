```
    _    _   _ ____ ___ ____  _     _____
   / \  | \ | / ___|_ _| __ )| |   | ____|
  / _ \ |  \| \___ \| ||  _ \| |   |  _|
 / ___ \| |\  |___) | || |_) | |___| |___
/_/   \_\_| \_|____/___|____/|_____|_____|
```

<!-- vim-markdown-toc GitLab -->

* [Setup Guide](#setup-guide)
  * [Pre-Commit Hook](#pre-commit-hook)
  * [Local Setup](#local-setup)
* [Upgrading Ansible](#upgrading-ansible)
  * [Ansible Required Python Packages](#ansible-required-python-packages)
  * [Upgrade Ansible Python Version](#upgrade-ansible-python-version)
* [Ansible-Vault Cheatsheet](#ansible-vault-cheatsheet)
* [Ansible Commands CheatSheet](#ansible-commands-cheatsheet)

<!-- vim-markdown-toc -->

# Setup Guide

## Pre-Commit Hook

Setup a pre-commit hook to ensure you pass tests before commiting:

``` bash
echo "" > .git/hooks/pre-commit
echo "#!/usr/bin/env bash" >> .git/hooks/pre-commit
echo "" >> .git/hooks/pre-commit
echo "./bin/lint.sh" >> .git/hooks/pre-commit
chmod u+x .git/hooks/pre-commit
```

## Local Setup

This setup guide assumes that you have pyenv installed on your system

1. Get the ansible version from `.tool-versions`

   ``` bash
   pyVersion=$(cat .tool-versions |awk -F ' ' '{print $2}')
   ```

2. Install python version via pyenv

   ``` bash
   pyenv install ${pyVersion}
   ```

3. Create an ansible virtualenv

   ``` bash
   pyenv virtualenv ${pyVersion} ansible
   ```

4. Activate `ansible` venv and install the required packages

   ```
   pyenv activate ansible
   pip install -r requirements.txt
   ```

For easy copy/paste:

``` shell
pyVersion=$(cat .tool-versions |awk -F ' ' '{print $2}')
pyenv install ${pyVersion}
pyenv virtualenv ${pyVersion} ansible
pyenv activate ansible
pip install -r requirements.txt
```

# Upgrading Ansible

* [Ansible release cycle](https://docs.ansible.com/ansible/latest/reference_appendices/release_and_maintenance.html#ansible-community-package-release-cycle)
* [Ansible-core Support Matrix](https://docs.ansible.com/ansible/latest/reference_appendices/release_and_maintenance.html#ansible-core-support-matrix)
* [Ansible-core Releases](https://github.com/ansible/ansible/releases)
* [Ansible Porting Guides](https://docs.ansible.com/ansible/7/porting_guides/porting_guides.html#porting-guides)

First, determine what version of `ansible-core` and `ansible` to upgrade to.  Make sure the versions are supported by the current python version being used (whatever the default version is on the current Ubuntu LTS).

Create a new virtualenv and backup the existing `requirements.txt`:

```
pyenv virtualenv 3.10.12 ansible-216
mv requirements.txt requirements-213.txt
touch requirements.txt
```

We will then begin updating all the packages listed in the [Minimum Required Python Packages Table](#ansible-required-python-packages).

Start with the `ansible` package (which will install the corresponding `ansible-core` package):

```
pip install ansible==9.1.0
```

```
$ pip freeze |grep "ansible"
ansible==9.1.0
ansible-core==2.16.2
```

Then, check all the other packages on [PyPi](https://pypi.org) to see if a newer version has been released (and is compatible with the version of python and ansible we are using):

```
pip install ansible-compat==4.1.10
pip install ansible-lint==6.22.1
pip install boto3 #also installs botocore
pip install netaddr
```

After installing all the **required** packages, update `requirements.txt`:

```
pip freeze > requirements.txt
```

Run ansible-lint locally (`./bin/test.sh`), and make sure there are no errors occuring due to the upgraded packages. Once ansible-lint is passing, commit your changes to your branch.

## Ansible Required Python Packages

The `requirements.txt` file contains **ALL** python packages that are installed for Ansible to run, but when upgrading Ansible, we want to install just the **MINIMUM** packages, and let pip pull in dependencies.

**Minimum Required Python Packages:**

| Package        |        Description        |        Why we need it        |
|----------------|:------------------------- |:-----------------------------|
| ansible-core   | Minimal runtime package containing a set of Ansible.Builtin | The actual ansible package |
| ansible        | “Batteries included” package, which adds a community-curated selection of Ansible Collections | Installs ansible-core and more importantly a bunch of ansible-galaxy collections |
| ansible-compat | Ansible compatibility goodies | Includes functions that facilitate working with various versions of Ansible 2.12 and newer |
| ansible-lint   | Checks playbooks for practices and behavior that could potentially be improved | ansible-lint is used in CI |
| boto3          | AWS SDK for Python | For the aws ansible collection to work |
| netaddr        | A network address manipulation library for Python | Makes ipaddr jinja filter work |

**Packages Required in user_data Terraform Module:**

(Packages that need to be installed and pinned to a specific version before `ansible-pull` cmd runs)

```
ansible
ansible-core
boto3
botocore
netaddr
```

## Upgrade Ansible Python Version

**When do we Need to Upgrade Python?**

We won't always need to bump the python version during each ansible upgrade, but we should always check the following:

* The python version being used (set in `.tool-versions`) matches the default Python version that is shipped with the latest Ubuntu LTS.  The reason being that most of our ec2 instances run on the latest Ubuntu LTS, and Ansible runs on instances locally during the initial provisioning in the user_data.
* The python version being used is supported by the `ansible-core` version we're moving to (and ideally is also supported by the current `ansible-core` version being used).  See the [ansible-core support matrix](https://docs.ansible.com/ansible/latest/reference_appendices/release_and_maintenance.html#ansible-core-support-matrix).

---

# Ansible-Vault Cheatsheet

For ease of use, the following I am using the following:

* `av` is aliased to `ansible-vault
* `ANSIBLE_VAULT_PASSWORD_FILE` env_var is set, so we don't need to pass in `--vault-id <vault-file>` constantly.

Encrypt a string:

```
av encrypt_string 'string_to_encrypt' --name 'string_name_of_variable'
```

Encrypt a file:

```
av encrypt file_name
```

View encrypted file:

```
av view file_name
```

Edit encrypted file:

```
av edit file_name
```

# Ansible Commands CheatSheet

* View Ansible Inventory:

  ```
  ansible-inventory -i inventory --graph
  ```

* Gather Facts:

  ```
  ansible -m setup all --limit "localhost"
  ```

* Check what hosts ansible-playbook will run against:

  ```
  ansible-playbook -e "hosts=localhost" playbooks/test-ansible.yml --check --list-hosts
  ```

